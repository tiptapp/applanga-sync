declare module 'applanga-sync' {
    export default class Applanga {
        constructor(any) {}
        configure(): Promise<void>;
        syncToPath(): Promise<void>;
    }
}
const regeneratorRuntime = require("regenerator-runtime");
const util = require('util');
require('util.promisify').shim();
const queryString = require('querystring');
const _ = require('lodash');
const glob = util.promisify(require('glob'));
const path = require('path');
const fs = require('fs');
const axios = require('axios');

class Applanga {
    constructor(config = {}) {
        const {
            appId,
            apiKey,
            url,
            groupName,
            tagIds,
            localePath,
            defaultLocale = 'en',
            logger,
            interval = 600000 // 10 mins
        } = config;
        this.translations = {};

        if (!appId || !apiKey || !url || !groupName || !localePath) {
            console.log('Applanga cannot be ran due to invalid initialization, require appId, apiKey, url, groupName, localePath');
        }

        this.logger = logger;
        this.config = {
            url,
            query: {
                app: appId
            },
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${apiKey}`
            },
            body: {
                options: {
                    onlyAsDraft: false,
                    onlyIfTextEmpty: false,
                    skipLockedTranslations: false
                }
            },
            groupName,
            tagIds,
            localePath,
            defaultLocale,
            syncTime: interval
        };
    }

    async configure() {
        try {
            let files = [];

            if (/\*\*/.test(this.config.localePath)) {
                files = await glob(`${this.config.localePath}/applanga.json`);
            } else {
                files = await glob(`${this.config.localePath}/*.json`);
            }

            for (const file of files) {
                let lang = path.basename(file, '.json');
                if (/\*\*/.test(this.config.localePath)) {
                    lang = path.dirname(file).split('/').pop();
                }
                this.translations[lang] = require(file);
            }
        } catch (error) {
            console.log(error);
        }
    }

    async syncToPath() {
        if (process.env.APPLANGA_SYNC_DISABLED === "true") {
            return;
        }
        const translations = await getFromApplanga(this);
        saveOrUpdate(this, translations);
    }

    async mergeLocalToApplanga() {
        const localTranslations = this.translations;
        const applangaTranslations = await getFromApplanga(this);

        const result = _.merge(applangaTranslations, localTranslations);

        await saveToApplanga(this, result);
    }

    async startSyncJob() {
        if (this.config.syncTime) {
            setInterval(async () => {
                try {
                    logInfo(this, 'Applanga sync job start');

                    if (process.env.APPLANGA_SYNC_DISABLED === "true") {
                        logWarn(this, `Applanga sync job skip due to process.env.APPLANGA_SYNC_DISABLED = ${process.env.APPLANGA_SYNC_DISABLED}`);
                        return;
                    }
                    const translations = await getFromApplanga(this);
                    saveOrUpdate(this, translations);

                    logInfo(this, 'Applanga sync job done');
                } catch (error) {
                    console.log(error);
                }
            }, this.config.syncTime);
        }
    }

    getTranslations() {
        return this.translations;
    }

    getTranslation(lang, id) {
        const defaultLang = _.get(this.config, 'defaultLocale');

        if (typeof this.translations[lang] !== 'object') {
            return this.translations[defaultLang][id];
        }

        if (typeof this.translations[lang][id] !== 'string') {
            return this.translations[defaultLang][id];
        }

        if (!this.translations[lang][id]) {
            return this.translations[defaultLang][id];
        }

        return this.translations[lang][id];
    }

    hasTranslation(lang, id) {
        return (
            typeof this.translations[lang] === 'object' &&
            typeof this.translations[lang][id] === 'string'
        );
    }

    isSupported(lang) {
        return Object.keys(this.translations).includes(lang);
    }

    defaultLocale() {
        return _.get(this.config, 'defaultLocale');
    }
}

function saveOrUpdate(_this, updates) {
    try {
        if (Object.keys(updates).length === 0) {
            logWarn(this, 'Applanga-sync job update skip due to empty data');
            return;
        }

        for (const lang in updates) {
            const update = updates[lang] || {};

            let writeDir = `${_this.config.localePath}`;
            let writePath = `${writeDir}/${lang}.json`;

            if (/\*\*/.test(_this.config.localePath)) {
                writeDir = `${_this.config.localePath.replace(/\*\*/,lang)}`;
                writePath = `${writeDir}/applanga.json`;
            }
            
            const exist = fs.existsSync(writeDir);
            if (!exist) {
                fs.mkdirSync(writeDir);
            }

            fs.writeFileSync(
                writePath,
                JSON.stringify(update, null, 4)
            );
            _this.translations[lang] = update;
        }
    } catch (err) {
        logError(this, `Applanga-sync job update locales file failed due to ${err.stack}`);

    }
}

function convertToApplangaData(_this, data) {
    const convertedData = {};

    for (const lang in data) {
        if (!convertedData[lang] && !_.isEmpty(data[lang])) {
            convertedData[lang] = {
                [_this.config.groupName]: {
                    entries: {}
                }
            };
        }
        for (const key in data[lang]) {
            convertedData[lang][_this.config.groupName].entries[key] = {
                v: data[lang][key]
            };
        }
    }

    return {
        options: _this.config.body.options,
        data: convertedData
    };
}

async function saveToApplanga(_this, translations) {
    try {
        if (!_this.config.query.app) {
            return;
        }
        const data = convertToApplangaData(_this, translations);
        let url = _this.config.url;

        if (_this.config.query) {
            const queries = queryString.stringify(_this.config.query);
            url = `${_this.config.url}?${queries}`;
        }

        await axios({
            url,
            data,
            method: 'POST',
            headers: _this.config.headers
        });
    } catch(err) {
        console.log(err);
    }

}

async function getFromApplanga(applanga) {
    try {
        if (!applanga.config.query.app) {
            logError(applanga, 'Applanga-sync job stop due to not providing appId');
            return;
        }

        const queries = queryString.stringify(applanga.config.query);
        
        let url = `${applanga.config.url}?${queries}&requestedGroups=["${applanga.config.groupName}"]`;

        if (applanga.config.tagIds) {
            url += `&requestedTagIds=[${applanga.config.tagIds.map(id => `"${id}"`).join(",")}]`;
        }
        
        const response = await axios({
            url,
            method: 'get',
            headers: applanga.config.headers
        });

        const data = _.get(response, 'data.data');
        if (!data) {
            logError(applanga, `Unexpected translation from Applanga, ${JSON.stringify(response.data)}`);
            return ;
        }

        const result = {};

        for (const [lang, langData] of Object.entries(data)) {
            const entries = _.get(
                langData,
                `${applanga.config.groupName}.entries`
            );

            result[lang] = result[lang] || {};

            if (!entries) {
                continue;
            }

            for (const key in entries) {
                const value = _.get(entries, [key, 'v']);
                if (value) {
                    result[lang][key] = value;
                }
            }
        }

        return result;
    } catch(err) {
        logError(applanga, `Applanga-sync had error, ${err.stack}`);
    }
}

function logInfo(_this, message) {
    if (_this && _this.logger && _this.logger.info) {
        _this.logger.info(message);
    }
}
function logWarn(_this, message) {
    if (_this && _this.logger && _this.logger.warn) {
        _this.logger.warn(message);
    }
}
function logError(_this, message) {
    if (_this && _this.logger && _this.logger.error) {
        _this.logger.error(message);
    }
}
module.exports = Applanga;
